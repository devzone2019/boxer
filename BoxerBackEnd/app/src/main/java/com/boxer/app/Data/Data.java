package com.boxer.app.Data;

import com.boxer.app.Models.Student;
import com.boxer.app.Repositories.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by Mak on 2019-10-01.
 */
@Component
public class Data implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public Data(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        initData();

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for(Student student : studentRepository.findAll()) {
            System.out.println(student.getName() + " " +
                    student.getSurname() + " " +
                    student.getMark());
        }
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    }

    public void initData() {
        Student student1 = new Student("Adam","Abacki", 4);
        Student student2 = new Student("Michał","Babacki", 3);
        Student student3 = new Student("Tomek","Cabacki", 5);
        Student student4 = new Student("Mariusz","Dbacki", 3);
        Student student5 = new Student("Piotr","Ebacki", 2);
        studentRepository.save(student1);
        studentRepository.save(student2);
        studentRepository.save(student3);
        studentRepository.save(student4);
        studentRepository.save(student5);
    }
}
