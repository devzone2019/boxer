package com.boxer.app.Models;

import javax.persistence.*;

/**
 * Created by Mak on 2019-10-01.
 */
@Entity
public class Student {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String surname;
    private Integer mark;

    public Student () {}

    public Student(String name, String surname, Integer mark) {
        this.name = name;
        this.surname = surname;
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }
}
