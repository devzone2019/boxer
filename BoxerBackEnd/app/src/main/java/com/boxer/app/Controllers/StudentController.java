package com.boxer.app.Controllers;

import com.boxer.app.Models.Student;
import com.boxer.app.Repositories.StudentRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by Mak on 2019-10-01.
 */
@RestController
public class StudentController {

    private StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/getAllStudents")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Collection<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @RequestMapping("/removeStudent/{studentId}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public void removeStudent(@PathVariable String studentId) {
        Student studentToRemove = studentRepository.findById(Long.parseLong(studentId)).get();
        studentRepository.delete(studentToRemove);
    }

    @RequestMapping("/addStudent/{name}/{surname}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public void addStudent(@PathVariable String name, @PathVariable String surname) {
        Student student = new Student(name, surname, 5);
        studentRepository.save(student);
    }

    @RequestMapping("/getStudentById/{studentId}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Student getStudentById(@PathVariable String studentId) {
        return studentRepository.findById(Long.parseLong(studentId)).get();
    }

    @RequestMapping("/updateStudent/{studentId}/{name}/{surname}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public void updateStudent(@PathVariable String studentId, @PathVariable String name, @PathVariable String surname) {
        Student student = studentRepository.findById(Long.parseLong(studentId)).get();
        student.setName(name);
        student.setSurname(surname);
        studentRepository.save(student);
    }
}
