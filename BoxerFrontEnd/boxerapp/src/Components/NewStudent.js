import React from 'react';
import { InputSection, InputTheme, AddButton, ErrorMessage } from '../Theme/Theme';
import { resolve, reject } from "q";

class NewStudent extends React.Component {

    state = {
        name: "",
        surname: "",
        validName: true,
        validSurname: true,
    }

    createStudent = () => {
        this.setState({validName: this.state.name.length > 0})
        this.setState({validSurname: this.state.surname.length > 0})
        console.log(this.state.name.length);
        if ( this.state.name.length > 0 && this.state.surname.length > 0) {
            
            fetch("http://localhost:8080/addStudent/" + this.state.name + "/" + this.state.surname).then(
                response => {
                    if(response.ok) {
                        resolve(response);
                        this.redirectToMainPage();
                    } else {
                        reject(response);
                    }
                }
            )
        }    
    }

    updateName = event => {
        this.setState({name: event.target.value})
    }

    updateSurname = event => {
        this.setState({surname: event.target.value})
    }

    redirectToMainPage = () => {
        this.props.history.push("/");
    }

    render() {
        return (
            <div>
                <InputSection className={!this.state.validName ? "HasError" : ""}>
                    <div>Name:</div>
                    <div><InputTheme onChange={this.updateName} value={this.state.name}/></div>
                    {!this.state.validName ? <ErrorMessage>Name is required</ErrorMessage> : ''}
                </InputSection>
                <InputSection className={!this.state.validSurname ? "HasError" : ""}>
                    <div>Surname:</div>
                    <div><InputTheme onChange={this.updateSurname} value={this.state.surname}/></div>
                    {!this.state.validName ? <ErrorMessage>Surname is required</ErrorMessage> : '' }
                </InputSection>
                <AddButton onClick={this.createStudent}>Add</AddButton>
            </div>
        )
    }
}

export default NewStudent