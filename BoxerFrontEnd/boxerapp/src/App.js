import React from 'react';
import './App.css';
import StudentsListComponent from './Components/StudentsList.js'
import NewStudentComponent from './Components/NewStudent.js'
import EditStudentComponent from './Components/EditStudent.js'
import NotFoundPathComponent from './Components/NotFoundPath.js'
import { Container } from './Theme/Theme'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

function App() {
  return (
    <Router>
      <Container>
        <Switch>
          <Route exact path="/" component={StudentsListComponent} />
          <Route exect path="/newStudent" component={NewStudentComponent} />
          <Route exect path="/editStudent/:studentId" component={EditStudentComponent} />
          <Route component={NotFoundPathComponent} />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
